<%@ page language="java" contentType="text/html; charset=windows-1256"
	pageEncoding="windows-1256" import="com.admin.login.LoginParameters"%>
<%
	response.setHeader("Cache-Control", "no-store,must-revalidate");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", -1);
	new java.util.Date();
	if (session.getAttribute("currentFacultySessionUser") != null) {
		LoginParameters facultyUser = (LoginParameters) (session.getAttribute("currentFacultySessionUser"));
		if (facultyUser != null) {
			//out.println("User already logged in");
			request.getSession().setAttribute("fac_name", facultyUser.getName());
		} else
			request.getSession(true).setAttribute("fac_name", facultyUser.getName());
	}
%>
<%@include file="header.jsp"%>
<%@include file="navbar.jsp"%>
<SCRIPT TYPE="text/javascript">
	var message = "Sorry, right-click has been disabled";
	function clickIE() {
		if (document.all) {
			(message);
			return false;
		}
	}
	function clickNS(e) {
		if (document.layers || (document.getElementById && !document.all)) {
			if (e.which == 2 || e.which == 3) {
				(message);
				return false;
			}
		}
	}
	if (document.layers) {
		document.captureEvents(Event.MOUSEDOWN);
		document.onmousedown = clickNS;
	} else {
		document.onmouseup = clickNS;
		document.oncontextmenu = clickIE;
	}
	document.oncontextmenu = new Function("return false")
</SCRIPT>
<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="jumbotron">
					<form class="form-horizontal" method="post" action="">
						<fieldset>
							<!-- Form Name -->
							<legend>ADD SUBJECTS</legend>
							<!-- Select Basic -->
							<div class="form-group">
								<label class="col-md-4 control-label" for="Course">Course
									Name</label>
								<div class="col-md-8">
									<select id="coursefullname" name="coursefullname"
										class="form-control" required="">
									</select>
								</div>
							</div>
							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-4 control-label" for="Course">Course Code</label>
								<div class="col-md-8">
									<select id="coursefullname" name="coursefullname"
										class="form-control" required="">
									</select>
								</div>
							</div>
							<!-- Select Basic -->
							<div class="form-group">
								<label class="col-md-4 control-label" for="semester">Course
									Semester</label>
								<div class="col-md-8">
									<select id="semester" name="semester" class="form-control"
										required="">
										<option value="one">1</option>
										<option value="two">2</option>
										<option value="three">3</option>
										<option value="four">4</option>
										<option value="one">5</option>
										<option value="two">6</option>
										<option value="three">7</option>
										<option value="four">8</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="Course">Department</label>
								<div class="col-md-8">
									<select id="coursefullname" name="coursefullname"
										class="form-control" required="">
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="Course">Credits</label>
								<div class="col-md-8">
									<select id="coursefullname" name="coursefullname"
										class="form-control" required="">
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="type">Type</label>
								<div class="col-md-8">
									<select id="semester" name="semester" class="form-control"
										required="">
										<option value="one">Core</option>
										<option value="two">Elective</option>
									</select>
								</div>

								<!-- Button -->
								<div class="form-group">
									<label class="col-md-4 control-label" for="type">Credits</label>
									<div class="col-md-8">
										<select id="semester" name="semester" class="form-control"
											required="">
											<option value="one">1</option>
											<option value="two">2</option>
											<option value="three">3</option>
											<option value="four">4</option>
										</select>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label" for="Course">Min
											Hour</label>
										<div class="col-md-8">
											<select id="coursefullname" name="coursefullname"
												class="form-control" required="">
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label" for="generate"></label>
										<div class="col-md-4">
											<input type="submit" id="generate" name="generate"
												class="btn btn-success" value="Add Subject">
										</div>
									</div>
						</fieldset>
					</form>

				</div>
			</div>
			<div class="col-lg-6">
				<div class="jumbotron">
					<?php include_once("timetable.list.php"); ?>
					Show here the list of subjects already added.</br> Show here the list of
					subjects already added.</br> Show here the list of subjects already
					added.</br> Show here the list of subjects already added.</br> Show here the
					list of subjects already added.</br> Show here the list of subjects
					already added.
				</div>
			</div>
		</div>

	</div>