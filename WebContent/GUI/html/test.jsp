<!-- right container -->
			<div id="right">
				<table id="table2">
					<colgroup>
						<col width="50"/>
						<col width="100"/>
						<col width="100"/>
						<col width="100"/>
						<col width="100"/>
						<col width="100"/>
					</colgroup>
					<tbody>
						<tr>
							<!-- if checkbox is checked, clone school subjects to the whole table row  -->
							<td class="redips-mark blank">
								<input id="week" type="checkbox" title="Apply subjects to the week" checked/>
								<input id="report" type="checkbox" title="Show subject report"/>
							</td>
							<td class="redips-mark dark">08:00</td>
							<td class="redips-mark dark">09:00</td>
							<td class="redips-mark dark">10:00</td>
							<td class="redips-mark dark">11:00</td>
							<td class="redips-mark dark">12:00</td>
							<td class="redips-mark dark">13:00</td>
							<td class="redips-mark dark">14:00</td>
							<td class="redips-mark dark">15:00</td>
							<td class="redips-mark dark">16:00</td>
							
						</tr>

						<% timetable(out,"Monday", 1); %>
						<% timetable(out,"Tuesday", 2); %>
						<% timetable(out,"Wednesday", 3); %>
						<% timetable(out,"Thursday", 4); %>
						<% timetable(out,"Friday", 5); %>
						<% timetable(out,"Saturday", 6); %> 
						
						<!--<?php timetable('14:00', 7) ?>
						<?php timetable('15:00', 8) ?>
						<?php timetable('16:00', 9) ?> -->
					</tbody>
				</table>
			</div><!-- right container -->