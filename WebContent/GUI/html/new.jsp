<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@include file="index.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		
		<meta name="description" content="Drag and drop table content with JavaScript"/>
		<meta name="viewport" content="width=device-width, user-scalable=no"/><!-- "position: fixed" fix for Android 2.2+ -->
		<link rel="stylesheet" href="style.css" type="text/css" media="screen"/>
		<script type="text/javascript">
			var redipsURL = '/javascript/drag-and-drop-example-3/';
		</script>
		<!--  <script type="text/javascript" src="../header.js"></script> -->
		<script type="text/javascript" src="../redips-drag-min.js"></script>
		<script type="text/javascript" src="script1.js"></script>
		<title>Timetable</title>
	</head>

<body>
	<center><h1 id="heading">Time Table</h1>
		</center>
		
	<center><p id="sem"> VI SEM CSE</p></center>
	
	<div id="main_container">
		<div id="redips-drag">
	
			<!-- left container (table with subjects) -->
			<div id="left">
					<table id="table1">
						<colgroup>
							<col width="190""/>
						</colgroup>
						<tbody>
							 <!-- 
							 <tr><td class="dark"><div id="ar" class="redips-drag redips-clone ar">Arts</div><input id="b_ar" class="ar" type="button" value="" onclick="report('ar')" title="Show only Arts"/></td></tr>
							  -->
							  <% subjects(out , 6) ; %>
							<tr><td class="redips-trash" title="Trash">Trash</td></tr>
							
						</tbody>
					</table>
					<br></br>
					<center><input type="button" name="download" value="download" style="width:130px; height:40px; background-color:#6386BD; color:white"></center>
					
				</div><!-- left container -->
		
				<!-- right container -->
				<div id="right">
					<table id="table2">
						<colgroup>
							<col width="50"/>
							<col width="100"/>
							<col width="100"/>
							<col width="100"/>
							<col width="100"/>
							<col width="100"/>
						</colgroup>
						<tbody>
							<tr>
								<!-- if checkbox is checked, clone school subjects to the whole table row  -->
								<td class="redips-mark blank">
									<input id="week" type="checkbox" title="Apply school subjects to the week" checked/>
									<input id="report" type="checkbox" title="Show subject report"/>
								</td>
								<td class="redips-mark dark">Monday</td>
								<td class="redips-mark dark">Tuesday</td>
								<td class="redips-mark dark">Wednesday</td>
								<td class="redips-mark dark">Thursday</td>
								<td class="redips-mark dark">Friday</td>

							</tr>
							<tr>
								<td class="redips-mark dark" rowspan=2>8:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
							</tr>
							<tr>
								<td class="redips-mark dark" rowspan=2>9:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
							</tr>
							<tr>
								<td class="redips-mark dark" rowspan=2>10:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
							</tr>
							<tr>
								<td class="redips-mark dark" rowspan=2>11:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
							</tr>
							<tr>
								<td class="redips-mark dark" rowspan=2>12:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
							</tr>
							<tr>
								<td class="redips-mark dark">13:00</td>
								<td class="redips-mark lunch" >Lunch</td>
								<td class="redips-mark lunch" >Lunch</td>
								<td class="redips-mark lunch" >Lunch</td>
								<td class="redips-mark lunch" >Lunch</td>
								<td class="redips-mark lunch" >Lunch</td>
							</tr>
							<tr>
								<td class="redips-mark dark" rowspan=2>14:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
							</tr>
							<tr>
								<td class="redips-mark dark" rowspan=2>15:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
							</tr>
							<tr>
								<td class="redips-mark dark" rowspan=2>16:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
								<td> <select> <option value="no-select">None</option>
											  <option value="room1">Room-1</option></select></td>
							</tr>
						</tbody>
					</table>
				</div><!-- right container -->
		
		</div>
	</div><!-- main container -->
	<!-- <br></br>
	<hr>
	</hr>
	<center><p style="font-size:15px">timetable@iiitkota</p></center>  -->
</body>
</html>