<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@include file="index.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<!-- <meta name="author" content="Darko Bunic"/> -->
		<meta name="description" content="Drag and drop table content with JavaScript"/>
		<meta name="viewport" content="width=device-width, user-scalable=no"/><!-- "position: fixed" fix for Android 2.2+ -->
		<link rel="stylesheet" href="style.css" type="text/css" media="screen"/>
		<script type="text/javascript">
			var redipsURL = '/javascript/drag-and-drop-example-3/';
		</script>
		<script type="text/javascript" src="../header.js"></script>
		<script type="text/javascript" src="../redips-drag-min.js"></script>
		<script type="text/javascript" src="script.js"></script>
		<title>Timetable</title>
	</head>
	<body>
		<div id="main_container">
			<!-- tables inside this DIV could have draggable content -->
			<div id="redips-drag">
	
				<!-- left container -->
				<div id="left">
					<table id="table1">
						<colgroup>
							<col width="190"/>
						</colgroup>
						<tbody>
						<tr><td class="dark"><div id="ar" class="redips-drag redips-clone ar">Arts</div><input id="b_ar" class="ar" type="button" value="" onclick="report('ar')" title="Show only Arts"/></td></tr>
							<%-- <% subjects(out,6); %> --%>
							<tr><td class="redips-trash" title="Trash">Trash</td></tr> 
							
							
						</tbody>
					</table>
				</div><!-- left container -->
				
				<!-- right container -->
				<div id="right">
					<table id="table2">
						<colgroup>
							<col width="50"/>
							<col width="100"/>
							<col width="100"/>
							<col width="100"/>
							<col width="100"/>
							<col width="100"/>
						</colgroup>
						<tbody>
							<tr>
								<!-- if checkbox is checked, clone school subjects to the whole table row  -->
								<td class="redips-mark blank">
									<input id="week" type="checkbox" title="Apply subjects to the week" checked/>
									<input id="report" type="checkbox" title="Show subject report"/>
								</td>
								<td class="redips-mark dark">Monday</td>
								<td class="redips-mark dark">Tuesday</td>
								<td class="redips-mark dark">Wednesday</td>
								<td class="redips-mark dark">Thursday</td>
								<td class="redips-mark dark">Friday</td>
							</tr>
                            
                            <tr>
								<td class="redips-mark dark">8:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="redips-mark dark">9:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="redips-mark dark">10:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="redips-mark dark">11:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="redips-mark dark">12:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="redips-mark dark">13:00</td>
								<td class="redips-mark lunch" >Lunch</td>
								<td class="redips-mark lunch" >Lunch</td>
								<td class="redips-mark lunch" >Lunch</td>
								<td class="redips-mark lunch" >Lunch</td>
								<td class="redips-mark lunch" >Lunch</td>
							</tr>
							<tr>
								<td class="redips-mark dark">14:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="redips-mark dark">15:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="redips-mark dark">16:00</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
                            
                            
<%-- 							<%  timetable(out,"08:00", 1); %>
							<% timetable(out,"09:00", 2); %>
							<% timetable(out,"10:00", 3); %>
							<% timetable(out,"11:00", 4); %>
							<% timetable(out,"12:00", 5); %>
							<!-- <tr>
								<td class="redips-mark dark">13:00</td>
								<td class="redips-mark lunch" colspan="5">Lunch</td>
							</tr> -->
							<% timetable(out,"3:00", 6); %>
							<% timetable(out,"14:00", 7); %>
							<% timetable(out,"15:00", 8); %>
							<% timetable(out,"16:00", 9); %>
 --%>							
						</tbody>
					</table>
				</div><!-- right container -->

			</div><!-- drag container -->
			<br/>
			
			<%-- <div id = "bottom">
					<table id="table0">
						<colgroup>
							<col width="190"/>
						</colgroup>
						<tbody>
							<tr><% subjects(out,6); %><tr>
						</tbody>
					</table>				
			</div>		 --%>	
			
			<div id="message">Drag subjects to the timetable (clone subjects with SHIFT key)</div>
			<div class="button_container">
				<input type="button" value="Save" class="button" onclick="save()" title="Save timetable"/>
			</div>
		</div><!-- main container -->
	</body>
</html>
