<%@ page language="java" contentType="text/html; charset=windows-1256"
	pageEncoding="windows-1256" import="com.admin.login.LoginParameters"%>
<%
	response.setHeader("Cache-Control", "no-store,must-revalidate");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", -1);
	new java.util.Date();
	if (session.getAttribute("currentFacultySessionUser") != null) {
		LoginParameters facultyUser = (LoginParameters) (session.getAttribute("currentFacultySessionUser"));
		if (facultyUser != null) {
			//out.println("User already logged in");
			request.getSession().setAttribute("fac_name", facultyUser.getName());
		} else
			request.getSession(true).setAttribute("fac_name", facultyUser.getName());
	}
%>
<%@include file="header.jsp"%>
<%@include file="navbar.jsp"%>
<SCRIPT TYPE="text/javascript">
	var message = "Sorry, right-click has been disabled";
	function clickIE() {
		if (document.all) {
			(message);
			return false;
		}
	}
	function clickNS(e) {
		if (document.layers || (document.getElementById && !document.all)) {
			if (e.which == 2 || e.which == 3) {
				(message);
				return false;
			}
		}
	}
	if (document.layers) {
		document.captureEvents(Event.MOUSEDOWN);
		document.onmousedown = clickNS;
	} else {
		document.onmouseup = clickNS;
		document.oncontextmenu = clickIE;
	}
	document.oncontextmenu = new Function("return false")
</SCRIPT>
<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="jumbotron">
					<form class="form-horizontal" method="post" action="">
						<fieldset>

							<!-- Form Name -->
							<legend>Add Classroom</legend>

							<!-- Select Basic -->
							<div class="form-group">
								<label class="col-md-4 control-label" for="Faculty">Room</label>
								<div class="col-md-8">
									<select id="facultyname" name="facultyname"
										class="form-control" required="">
									</select>
								</div>
							</div>
							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-4 control-label" for="Faculty">Room Code</label>
								<div class="col-md-8">
									<select id="dept" name="dept"
										class="form-control" required="">
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="Faculty">Location</label>
								<div class="col-md-8">
									<select id="fcode" name="fcode"
										class="form-control" required="">
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="Faculty">Capacity</label>
								<div class="col-md-8">
									<select id="ftype" name="ftype"
										class="form-control" required="">
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="Faculty">Projector</label>
								<div class="col-md-8">
									<select id="designation" name="designation"
										class="form-control" required="">
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="Faculty">Type</label>
								<div class="col-md-8">
									<select id="contact" name="contact"
										class="form-control" required="">
									</select>
								</div>
							</div>
														<div class="form-group">
								<label class="col-md-4 control-label" for="generate"></label>
								<div class="col-md-4">
									<input type="submit" id="generate" name="generate"
										class="btn btn-success" value="Add Classroom">
								</div>
							</div>

						</fieldset>
					</form>

				</div>
			</div>
			<div class="col-lg-6">
				<div class="jumbotron">
					Show here the list of teachers already added.
				</div>
			</div>
		</div>

	</div>